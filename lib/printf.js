var attributes = require('./attributes.json');
var background = require('./background.json');
var fontcolor = require('./fontcolor.json');
var type = require('./type.json');

function mixcolor(selectedattribute,selectedbackground,selectedfontcolor){
    var resetColor= '\x1b[0m'
    var color = '\033['+attributes[selectedattribute]+';'+background[selectedbackground]+';'+fontcolor[selectedfontcolor]+'m'
    var messagecolor = color+'%s'+resetColor
    return messagecolor
}

module.exports = {
    printf:(message,selectedattribute,selectedbackground,selectedfontcolor)=>{
        var color
        let selectedtype=selectedattribute
        if (!selectedtype){
            selectedtype="normal"
            color = mixcolor(type[selectedtype].attributes,type[selectedtype].background,type[selectedtype].fontcolor)
        }
        else if(selectedbackground && selectedfontcolor){
            color = mixcolor(selectedattribute,selectedbackground,selectedfontcolor)
        }else{
            color = mixcolor(type[selectedtype].attributes,type[selectedtype].background,type[selectedtype].fontcolor)
        }
        console.log(color,message)
    }
}

