
function printfp(message,a,b,c){
    let resetColor= '\x1b[0m'
    let attributes={
        normal: '0',
        bold: '1',
        underline: '4',
        blink: '5',
        reverse: '7',
        invisible: '8'
    }
    let background ={
        black: '30',
        red: '31',  
        green: '32',
        yellow: '33',
        blue: '34',
        magenta: '35',
        cyan: '36',
        white: '37'
    }
    let fontcolor = {
        black: '40',
        red: '41',
        green: '42',
        yellow: '43',
        blue: '44',
        magenta: '45',
        cyan: '46',
        white: '47'
    }
    let color = '\033['+attributes[a]+';'+background[b]+';'+fontcolor[c]+'m'
    let messagecolor = color+'%s'+resetColor
    console.log(messagecolor,message)
}

printfp("hola","normal","yellow","red")

console.log("tu mama me toca")


